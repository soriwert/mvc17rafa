<?php
/**
*
*/
require_once('models/User.php');

class UserController
{

    function __construct()
    {
        echo "En UserController";
    }

    public function render($view, $data)
    {
        // $users = $data;
        require("views/$view");
    }

    //lista de Users
    public function index()
    {
        // die('index');
        $users = User::all();
        // var_dump($users);

        //parametros para la vista
        //luego lo haremos sin usar variable
        // $this->render('users.index.php', ['users' => $users]);
        $data = array('users' => $users);
        $this->render('users.index.php', $data);
    }

    //detalle de un User
    public function show($id)
    {
        $user = User::find($id);
        // var_dump($user);
        $this->render('users.show.php', ['user' => $user]);
    }

    public function delete($id)
    {
        die('borre el user ' . $id);
    }

    public function create()
    {
        die('formulario de alta');
    }

    public function store()
    {
        die('recoger formulario anterior');
    }

    //Mostrar formulario con datos previos
    public function edit($id)
    {
        $user = User::find($id);
        $this->render('users.edit.php',
            ['user' => $user]);
    }

    public function update($id)
    {
        $user = User::find($id);

        $user->name = $_POST['name'];
        $user->surname = $_POST['surname'];
        $user->age = $_POST['age'];
        $user->email = $_POST['email'];

        $user->save();
        header('location: /mvc17/v4/user');
    }
}
